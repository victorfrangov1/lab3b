public class Lion{
    public String name;
    public double age;
    public int numOfLegs;

    public void sleep(){
        System.out.println(this.name + " is sleeping on his " + this.numOfLegs + " legs.");
    }

    public void age(){
        System.out.println(this.name + " is " + this.age + " years old.");
    }
}