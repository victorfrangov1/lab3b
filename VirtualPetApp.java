import java.util.Scanner;
public class VirtualPetApp{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);

        Lion[] packOfLions = new Lion[4];
        Lion animal = new Lion();

        for(int i = 0; i < packOfLions.length; i++){
            packOfLions[i] = new Lion();
            
            System.out.println("");
            System.out.println("What will be the name of your lion? ");
            packOfLions[i].name = reader.nextLine();
            System.out.println("What is his age? ");
            packOfLions[i].age = Double.parseDouble(reader.nextLine());
            System.out.println("How many legs does he have? ");
            packOfLions[i].numOfLegs = Integer.parseInt(reader.nextLine());
        }
        System.out.println(packOfLions[3].name + ", " + packOfLions[3].age + " years old, " + packOfLions[3].numOfLegs + " legs.");
    
        packOfLions[0].sleep();
        packOfLions[0].age();
    }
}